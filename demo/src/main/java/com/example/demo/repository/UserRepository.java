package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.entity.User;

public interface UserRepository extends JpaRepository<User, Long>{

	@Query("FROM User e where e.user_name=:username")
	User findByUsername(@Param("username")String username);
	
	@Query("FROM User e where e.email=:email")
	User findByEmail(@Param("email") String email);

}
