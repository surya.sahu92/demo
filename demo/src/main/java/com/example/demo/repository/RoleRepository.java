package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.entity.Role;

public interface RoleRepository extends JpaRepository<Role, Long>{

	@Query("FROM Role u where u.id=:role")
	Role findByRoleId(@Param("role")Long role);

}
