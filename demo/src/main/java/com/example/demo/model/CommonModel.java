package com.example.demo.model;

import java.util.Date;

import io.swagger.annotations.ApiModelProperty;

public class CommonModel {

	@ApiModelProperty(notes = "This should be created user id")
	Long created_by;
	@ApiModelProperty(notes = "This should be created date")
	Date created_date;
	@ApiModelProperty(notes = "This should be updated user id")
	Long updated_by;
	@ApiModelProperty(notes = "This should be updated date")
	Date updated_date;
	
	public Long getCreated_by() {
		return created_by;
	}
	public void setCreated_by(Long created_by) {
		this.created_by = created_by;
	}
	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}
	public Long getUpdated_by() {
		return updated_by;
	}
	public void setUpdated_by(Long updated_by) {
		this.updated_by = updated_by;
	}
	public Date getUpdated_date() {
		return updated_date;
	}
	public void setUpdated_date(Date updated_date) {
		this.updated_date = updated_date;
	}
	
	
}
