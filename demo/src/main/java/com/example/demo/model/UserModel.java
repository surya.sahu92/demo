package com.example.demo.model;

import io.swagger.annotations.ApiModelProperty;

public class UserModel extends CommonModel{
	@ApiModelProperty(notes = "The database generated product ID")
	Long id;
	@ApiModelProperty(notes = "This should be user full name")
	String name;
	@ApiModelProperty(notes = "This should be user login name")
	String user_name;
	@ApiModelProperty(notes = "This should be user login password")
	String password;
	@ApiModelProperty(notes = "This should be user email")
	String email;
	@ApiModelProperty(notes = "This is optional")
	Boolean is_enabled;
	@ApiModelProperty(notes = "This is optional")
	Boolean is_account_non_locked;
	@ApiModelProperty(notes = "This is optional")
	Boolean is_confirm;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Boolean getIs_enabled() {
		return is_enabled;
	}
	public void setIs_enabled(Boolean is_enabled) {
		this.is_enabled = is_enabled;
	}
	public Boolean getIs_account_non_locked() {
		return is_account_non_locked;
	}
	public void setIs_account_non_locked(Boolean is_account_non_locked) {
		this.is_account_non_locked = is_account_non_locked;
	}
	public Boolean getIs_confirm() {
		return is_confirm;
	}
	public void setIs_confirm(Boolean is_confirm) {
		this.is_confirm = is_confirm;
	}
	
	
}
