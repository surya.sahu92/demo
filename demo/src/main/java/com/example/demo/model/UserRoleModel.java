package com.example.demo.model;

import com.example.demo.entity.Role;
import com.example.demo.entity.User;

public class UserRoleModel {
	Long id;
	User user;
	Role role;
	Boolean is_primary;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	}
	public Boolean getIs_primary() {
		return is_primary;
	}
	public void setIs_primary(Boolean is_primary) {
		this.is_primary = is_primary;
	}
}
