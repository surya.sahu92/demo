package com.example.demo.utility;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

/**
 * 
 *Logging Aspect to log all the exception in a file
 *
 */
@Aspect
@Component
public class LoggingAspect {
	
	/**
	 * Calls log() to Log repository Exception
	 * @param exception
	 * @throws Exception
	 */
	@AfterThrowing(pointcut = "execution(* com.example.carrental.repository.*.*(..))", throwing = "exception")
	public void logRepositoryException(Exception exception) throws Exception {
		log(exception);
	}
	
	/**
	 * Calls log() to Log Service exception
	 * @param exception
	 * @throws Exception
	 */
	@AfterThrowing(pointcut = "execution(* com.example.carrental.service.*Impl.*(..))", throwing = "exception")
	public void logServiceException(Exception exception) throws Exception {
		log(exception);
	}
	
	/**
	 * Logs exception
	 * @param exception
	 */
	public void log(Exception exception) {
		Logger logger = LogManager.getLogger(this.getClass());
		logger.error(exception.getMessage(),exception);
	}
}
