package com.example.demo.service;

import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.example.demo.config.MediUser;
import com.example.demo.entity.Role;
import com.example.demo.entity.User;
import com.example.demo.entity.UserRole;
import com.example.demo.repository.RoleRepository;
import com.example.demo.repository.UserRepository;
import com.example.demo.repository.UserRoleRepository;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	UserRepository userRepository;
	@Autowired
	UserRoleRepository userRoleRepository;
	@Autowired
	RoleRepository roleRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepository.findByUsername(username);
		UserRole userRole = userRoleRepository.findByUserId(user.getId());
		Role role = roleRepository.findByRoleId(userRole.getRole());
		GrantedAuthority authority = new SimpleGrantedAuthority(role.getRole_name());
		MediUser mediUser = new MediUser(user.getUser_name(),user.getPassword(),user.getIs_enabled(),
				true,true,user.getIs_account_non_locked(),Arrays.asList(authority),user.getId(),user.getName(),user.getEmail(),
				role.getDisplay_name());
		return mediUser;
	}

}
