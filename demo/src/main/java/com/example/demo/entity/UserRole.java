package com.example.demo.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="t_tutorial_user_role")
public class UserRole implements Serializable{

	private static final long serialVersionUID = -5790936923686647284L;
	
	@Id
	Long id;
	@Column(name="user_id")
	Long user;
	
	@Column(name="role_id")
	Long role;
	
	Boolean is_primary;
	public Long getId() {
		return id;
	} 
	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getUser() {
		return user;
	}
	public void setUser(Long user) {
		this.user = user;
	}
	
	public Long getRole() {
		return role;
	}
	public void setRole(Long role) {
		this.role = role;
	}
	public Boolean getIs_primary() {
		return is_primary;
	}
	public void setIs_primary(Boolean is_primary) {
		this.is_primary = is_primary;
	}
	
	
	
	
}
