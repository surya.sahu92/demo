package com.example.demo.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.LocalDateTime;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.config.MediUser;
import com.example.demo.config.SessionManagement;
import com.example.demo.model.UserModel;
import com.example.demo.utility.GlobalResponseUtil;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/restapi/admin")
public class AdminController {
	@Autowired
	SessionManagement sessionManagement;
	
	@ApiOperation(value = "Get session for user")
	@RequestMapping(value="/session", method=RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<?> registerUser(UserModel userModel) throws Exception{
		MediUser mediUser = sessionManagement.getSession();
		return GlobalResponseUtil.generateResponse(LocalDateTime.now(), HttpStatus.OK, "TRUE", mediUser);
	}
	
	@RequestMapping("/report")
    void getFile(HttpServletResponse response) throws IOException {

        String fileName = "report.pdf";
        String path = "///C:///Users///SURYA///Desktop///sample.pdf";

        File file = new File(path);
        FileInputStream inputStream = new FileInputStream(file);

        response.setContentType("application/pdf");
        response.setContentLength((int) file.length());
        response.setHeader("Content-Disposition", "inline;filename=\"" + fileName + "\"");

        FileCopyUtils.copy(inputStream, response.getOutputStream());

    }
	
}
