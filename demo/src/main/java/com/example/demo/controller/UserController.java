package com.example.demo.controller;

import java.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.config.MediUser;
import com.example.demo.config.SessionManagement;
import com.example.demo.model.UserModel;
import com.example.demo.utility.GlobalResponseUtil;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/restapi/user")
public class UserController {
	@Autowired
	SessionManagement sessionManagement;
	
	
	@ApiOperation(value = "Get session for user")
	@RequestMapping(value="/session", method=RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<?> registerUser(UserModel userModel) throws Exception{
		MediUser mediUser = sessionManagement.getSession();
		return GlobalResponseUtil.generateResponse(LocalDateTime.now(), HttpStatus.OK, "TRUE", mediUser);
	}
}
